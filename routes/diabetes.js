var express = require('express');
var router  = express.Router();
var exec = require('child_process').exec;
var fs = require('fs');
var bodyParser = require('body-parser')
var jsonParser = bodyParser.json();

router.post('/get-resultados',jsonParser,function(req,res){
    console.log(req.body)
    let body = req.body
    let jsonFile = {
        "muestras":
            [
                {
                    "Embarazos":parseFloat(body.embarazos),
                    "Glucosa":parseFloat(body.glucosa),
                    "PresiónSanguínea":parseFloat(body.presionSanguinea),
                    "GrosorDeLaPiel":parseFloat(body.grosorPiel),
                    "Insulina":parseFloat(body.insulina),
                    "IMC":parseFloat(body.imc),
                    "FunciónDePedigríDiabetes":parseFloat(body.pedigri),
                    "Edad":parseFloat(body.edad)
                }
            ]
    }
    fs.writeFile("inputs_data.json",JSON.stringify(jsonFile), function(err) {
        exec("jupyter nbconvert --to notebook --execute diabetes_server.ipynb", (error, stdout, stderr)=>{
            console.log(stdout)
            fs.readFile('resultados_entrenamiento.json', 'utf8', function(err, contents) {
                res.header("Content-Type",'application/json');
                res.send(contents);
            });    
        })
    }); 

});

module.exports = router;

